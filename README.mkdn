CyanPop 6.0.1
====================================

### Installing Repo ###
```bash
# Make a directory where Repo will be stored and add it to the path
    $ mkdir ~/.bin
    $ PATH=~/.bin:$PATH

# Download Repo itself
    $ curl https://storage.googleapis.com/git-repo-downloads/repo > ~/.bin/repo

# Make Repo executable
    $ chmod a+x ~/.bin/repo
```

### Initializing Repo ###
```bash
### CyanPop sources: ###
    $ repo init -u git://github.com/CyanPop/platform_manifest.git -b cm-13.0
```
### For sync: ###
```bash
    $ repo sync -j4
```
### To build for your device.. ###
```bash
    $ . build/envsetup.sh
    $ breakfast device_name
    $ mka bacon 
```

### Device maintainers and developer: ###
```bash
    $ Rom Developer   (RolanDroid)
    $ Moto G          (RolanDroid)
    $ Moto G 4G       (RolanDroid)
    $ Moto G 2014     (RolanDroid) 
    $ Moto G 2014 LTE (RolanDroid)
    $ Moto E 2015     (RolanDroid)
    $ Moto E          (RolanDroid)
    $ Moto X 2014     (RolanDroid)
    $ Galaxy S3       (RolanDroid)
    $ OnePlus One     (RolanDroid)
    $ OnePlus X       (RolanDroid)
    $ Google Nexus 5  (RolanDroid)
```

### Credits: ###
```bash
    $ RolanDroid (Rom Developer)
    $ CyanogenMod
    $ CrDroid
    $ Temasek
    $ @PartimusPrime (for his bootanimations)
```




